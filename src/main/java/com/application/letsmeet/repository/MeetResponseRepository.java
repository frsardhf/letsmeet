package com.application.letsmeet.repository;

import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.model.LetsMeetResponse;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeetResponseRepository extends JpaRepository<LetsMeetResponse, Long> {
    List<LetsMeetResponse> findByEvent(LetsMeetEvent event);
}
