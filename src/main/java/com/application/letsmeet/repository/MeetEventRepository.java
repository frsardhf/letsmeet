package com.application.letsmeet.repository;

import com.application.letsmeet.model.LetsMeetEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeetEventRepository extends JpaRepository<LetsMeetEvent, Long> {
}
