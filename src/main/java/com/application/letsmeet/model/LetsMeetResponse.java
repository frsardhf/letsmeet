package com.application.letsmeet.model;

import java.util.List;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "lets_meet_response")
@Data
@NoArgsConstructor
public class LetsMeetResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "response_id")
    private Long id;

    @Column(name = "username")
    private String username;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private LetsMeetEvent event;

    @ElementCollection
    @Column(name = "selected_time")
    private List<String> selectedTime;
}