package com.application.letsmeet.controller;

import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.model.LetsMeetResponse;
import com.application.letsmeet.service.MeetEventService;
import com.application.letsmeet.service.MeetResponseService;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class LetsMeetController {

    @Autowired
    private MeetEventService meetEventService;

    @Autowired
    private MeetResponseService meetResponseService;

    /**
     * Get form.
     * @param model model
     * @return Form
     */
    @GetMapping(path = "", produces = {"application/json"})
    public String letsMeetForm(Model model) {
        model.addAttribute("event", new LetsMeetEvent());
        return "letsmeet/letsMeet";
    }

    /**
     * Post form.
     * @param event meet event
     * @param model model
     * @return Meet event details
     */
    @PostMapping(path = "", produces = {"application/json"})
    public String letsMeetSubmit(@ModelAttribute LetsMeetEvent event, Model model) {
        meetEventService.createMeetEvent(event);
        model.addAttribute("event", event);
        model.asMap().clear();
        return "redirect:/details/" + event.getId();
    }

    /**
     * Get meet event details.
     * @param id meet event id
     * @param model model
     * @return Meet event details view
     */
    @GetMapping(path = "/details/{id}", produces = {"application/json"})
    public String getMeetEvent(@PathVariable(value = "id") Long id, Model model) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        List<List<String>> timeslots = meetEventService.getPartitionedMeetEventTimeSlots(event);
        model.addAttribute("event", event);
        model.addAttribute("timeslots", timeslots);
        return "letsmeet/details";
    }

    /**
     * Update meet event.
     * @param id meet event id
     * @param startTime new meet event start time
     * @param endTime new meet event end time
     * @param model model
     * @return Meet event details
     */
    @PostMapping(path = "/update/{id}", produces = {"application/json"})
    public String updateMeetEvent(@PathVariable(value = "id") Long id,
                                  @RequestParam("newStartTime") String startTime,
                                  @RequestParam("newEndTime") String endTime,
                                  Model model) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        meetEventService.updateEvent(startTime, endTime, event);
        meetResponseService.deleteAllResponse(id);
        model.addAttribute("event", event);
        model.asMap().clear();
        return "redirect:/details/" + event.getId();
    }

    /**
     * Post meet event response.
     * @param id meet event id
     * @param idTimes selected time response
     * @param model model
     * @return Meet event response
     */
    @PostMapping(path = "/details/{id}", produces = {"application/json"})
    public String letsMeetResponseSubmit(@PathVariable(value = "id") Long id,
                                         @RequestParam("timeChecked") List<String> idTimes,
                                         @RequestParam("name") String name,
                                         Model model) {
        ArrayList<String> selectedTime = new ArrayList<>();
        if (idTimes != null) {
            for (String idTime : idTimes) {
                selectedTime.add(idTime);
            }
        }
        LetsMeetResponse response = new LetsMeetResponse();
        String username = name;
        Iterable<LetsMeetResponse> responses = meetResponseService.getListResponseByEvent(id);
        meetResponseService.deleteAllResponseByUsername(responses, username);
        response = meetResponseService.createMeetResponse(id, username, selectedTime, response);
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        model.addAttribute("response", response);
        model.asMap().clear();
        return "redirect:/responses/" + event.getId();
    }

    /**
     * Get meet event response.
     * @param id meet event id
     * @param model model
     * @return Meet event response view
     */
    @GetMapping(path = "/responses/{id}", produces = {"application/json"})
    public String getMeetEventResponse(@PathVariable(value = "id") Long id,
                                       Model model) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        Iterable<LetsMeetResponse> responses = meetResponseService.getListResponseByEvent(id);
        TreeMap<String, List<String>> map = meetResponseService.getResponses(responses, event);
        model.addAttribute("event", event);
        model.addAttribute("map", map);
        return "letsmeet/responses";
    }

    /**
     * List of hour.
     * @return hours
     */
    @ModelAttribute("hourList")
    public List<String> getHourList() {
        List<String> hourList = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            StringBuilder sb = new StringBuilder();
            if (i < 10) {
                sb.append("0" + i + ":00");
            } else {
                sb.append(i + ":00");
            }
            hourList.add(sb.toString());
        }
        return hourList;
    }
}
