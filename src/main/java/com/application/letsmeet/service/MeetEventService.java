package com.application.letsmeet.service;

import com.application.letsmeet.model.LetsMeetEvent;
import java.util.List;

public interface MeetEventService {
    LetsMeetEvent createMeetEvent(LetsMeetEvent event);

    LetsMeetEvent getMeetEventById(Long id);

    LetsMeetEvent updateEvent(String start, String end, LetsMeetEvent event);

    List<String> getMeetEventTimeSlots(LetsMeetEvent event);

    List<List<String>> getPartitionedMeetEventTimeSlots(LetsMeetEvent event);
}
