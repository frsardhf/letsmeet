package com.application.letsmeet.service;

import com.application.letsmeet.core.CalendarSlots;
import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.repository.MeetEventRepository;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MeetEventServiceImpl implements MeetEventService {
    private CalendarSlots calendarSlots;

    @Autowired
    MeetEventRepository meetEventRepository;

    public MeetEventServiceImpl() {
        this.calendarSlots = new CalendarSlots();
    }

    @Override
    public LetsMeetEvent createMeetEvent(LetsMeetEvent event) {
        meetEventRepository.save(event);
        return event;
    }

    @Override
    public LetsMeetEvent getMeetEventById(Long id) {
        return meetEventRepository.findById(id).get();
    }

    @Override
    public LetsMeetEvent updateEvent(String start, String end, LetsMeetEvent event) {
        String timePattern = "HH:mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        event.setStartTime(LocalTime.parse(start, timeFormatter));
        event.setEndTime(LocalTime.parse(end, timeFormatter));
        meetEventRepository.save(event);
        return event;
    }

    @Override
    public List<String> getMeetEventTimeSlots(LetsMeetEvent event) {
        return calendarSlots.getTimeSlots(event);
    }

    @Override
    public List<List<String>> getPartitionedMeetEventTimeSlots(LetsMeetEvent event) {
        return calendarSlots.getPartitionedTimeSlots(calendarSlots.getTimeSlots(event));
    }
}
