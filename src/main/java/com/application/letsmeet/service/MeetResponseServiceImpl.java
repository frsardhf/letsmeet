package com.application.letsmeet.service;

import com.application.letsmeet.core.CalendarResponses;
import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.model.LetsMeetResponse;
import com.application.letsmeet.repository.MeetResponseRepository;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MeetResponseServiceImpl implements MeetResponseService {
    private CalendarResponses calendarResponses;

    @Autowired
    MeetResponseRepository meetResponseRepository;

    @Autowired
    MeetEventService meetEventService;

    public MeetResponseServiceImpl() {
        this.calendarResponses = new CalendarResponses();
    }

    @Override
    public LetsMeetResponse createMeetResponse(Long id,
                                               String username,
                                               List<String> selectedTime,
                                               LetsMeetResponse response) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        response.setEvent(event);
        response.setUsername(username);
        response.setSelectedTime(selectedTime);
        meetResponseRepository.save(response);
        return response;
    }

    @Override
    public void deleteResponse(LetsMeetResponse response) {
        meetResponseRepository.delete(response);
    }

    @Override
    public Iterable<LetsMeetResponse> getListResponseByEvent(Long id) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        List<LetsMeetResponse> responses = meetResponseRepository.findByEvent(event);
        return responses;
    }

    @Override
    public TreeMap<String, List<String>> getResponses(Iterable<LetsMeetResponse> responses,
                                                      LetsMeetEvent event) {
        Map<String, List<String>> map = calendarResponses.getResponses(responses, event);
        TreeMap<String, List<String>> sorted = new TreeMap<>(map);
        return sorted;
    }

    @Override
    public void deleteAllResponse(Long id) {
        Iterable<LetsMeetResponse> responses = getListResponseByEvent(id);
        for (LetsMeetResponse response : responses) {
            deleteResponse(response);
        }
    }

    /**
     * deleteAllResponseByUsername.
     */
    @Override
    public void deleteAllResponseByUsername(Iterable<LetsMeetResponse> responses, String username) {
        for (LetsMeetResponse response : responses) {
            if (response.getUsername().equals(username)) {
                deleteResponse(response);
            }
        }
    }
}
