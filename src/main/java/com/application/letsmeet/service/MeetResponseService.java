package com.application.letsmeet.service;

import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.model.LetsMeetResponse;
import java.util.List;
import java.util.TreeMap;

public interface MeetResponseService {
    LetsMeetResponse createMeetResponse(Long id,
                                        String username,
                                        List<String> selectedTime,
                                        LetsMeetResponse response);

    Iterable<LetsMeetResponse> getListResponseByEvent(Long id);

    TreeMap<String, List<String>> getResponses(Iterable<LetsMeetResponse> responses,
                                               LetsMeetEvent event);

    void deleteResponse(LetsMeetResponse response);

    void deleteAllResponse(Long id);

    void deleteAllResponseByUsername(Iterable<LetsMeetResponse> responses, String username);
}


