package com.application.letsmeet.core;

import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.model.LetsMeetResponse;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalendarResponses implements Calendar {
    private CalendarSlots calendarSlots;
    private LocalTime startTime;
    private LocalTime endTime;

    public CalendarResponses() {
        this.calendarSlots = new CalendarSlots();
    }

    /**
     * Get empty map.
     * @param event meet event
     * @return empty map
     */
    public Map<String, List<String>> getMapResponse(LetsMeetEvent event) {
        startTime = event.getStartTime();
        endTime = event.getEndTime();
        Map<String, List<String>> map = new HashMap();
        List<String> slots = calendarSlots.getTimeSlots(event);
        for (String slot : slots) {
            List<String> attendees = new ArrayList<>();
            map.put(slot, attendees);
        }
        return map;
    }

    /**
     * Populate map with responses.
     * @param responses meet event responses
     * @param event meet event
     * @return map of responses
     */
    public Map<String, List<String>> getResponses(Iterable<LetsMeetResponse> responses,
                                                  LetsMeetEvent event) {
        Map<String, List<String>> mapResult = getMapResponse(event);
        for (LetsMeetResponse response : responses) {
            for (String time : response.getSelectedTime()) {
                mapResult.get(time).add(response.getUsername());
            }
        }
        return mapResult;
    }

    @Override
    public LocalTime getStartTime() {
        return startTime;
    }

    @Override
    public LocalTime getEndTime() {
        return endTime;
    }
}
