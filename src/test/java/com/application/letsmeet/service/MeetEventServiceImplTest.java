package com.application.letsmeet.service;

import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.repository.MeetEventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MeetEventServiceImplTest {
    @Mock
    private MeetEventRepository meetEventRepository;

    @InjectMocks
    private MeetEventServiceImpl meetEventService;

    private LetsMeetEvent letsMeetEvent;

    /**
     * setUp.
     */
    @BeforeEach
    public void setUp() {
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("135135"));
        letsMeetEvent.setName("Group Project Microservice");
        letsMeetEvent.setDate(LocalDate.parse("2021-05-21"));
        String timePattern = "HH:mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("08:00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("13:00", timeFormatter));
    }

    @Test
    public void testServiceCreateEvent() {
        lenient().when(meetEventService.createMeetEvent(letsMeetEvent)).thenReturn(letsMeetEvent);
    }

    @Test
    public void testServiceGetEventById() {
        Optional<LetsMeetEvent> event = Optional.of(letsMeetEvent);
        when(meetEventRepository.findById(letsMeetEvent.getId()))
                .thenReturn(event);
        LetsMeetEvent eventResult = meetEventService.getMeetEventById(letsMeetEvent.getId());

        assertEquals(letsMeetEvent, eventResult);
        assertEquals(letsMeetEvent.getResponses(), eventResult.getResponses());
        assertEquals(letsMeetEvent.getName(), eventResult.getName());
        assertEquals(letsMeetEvent.getDate(), eventResult.getDate());

        List<String> timeSlots = meetEventService.getMeetEventTimeSlots(eventResult);
        List<List<String>> partitioned = meetEventService
                .getPartitionedMeetEventTimeSlots(eventResult);
        assertEquals(meetEventService
                .getMeetEventTimeSlots(letsMeetEvent).size(), timeSlots.size());
        assertEquals(meetEventService
                .getPartitionedMeetEventTimeSlots(letsMeetEvent).size(), partitioned.size());
    }

    @Test
    public void testServiceUpdateEvent() {
        meetEventService.createMeetEvent(letsMeetEvent);
        String start = "08:00";
        String end = "11:00";
        String timePattern = "HH:mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        LocalTime currentEnd = letsMeetEvent.getEndTime();
        letsMeetEvent.setEndTime(LocalTime.parse("11:00", timeFormatter));
        lenient().when(meetEventService.updateEvent(start, end, letsMeetEvent))
                .thenReturn(letsMeetEvent);
        LetsMeetEvent resultEvent = meetEventService.updateEvent(start, end, letsMeetEvent);
        assertNotEquals(resultEvent.getEndTime(), currentEnd);
    }
}
