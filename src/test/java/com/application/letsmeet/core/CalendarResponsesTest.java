package com.application.letsmeet.core;

import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.model.LetsMeetResponse;
import com.application.letsmeet.repository.MeetResponseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(MockitoExtension.class)
public class CalendarResponsesTest {
    @Mock
    private MeetResponseRepository meetResponseRepository;

    private CalendarSlots calendarSlots;
    private CalendarResponses calendarResponses;
    private LetsMeetEvent letsMeetEvent;
    private LetsMeetResponse letsMeetResponse;

    /**
     * Set up test.
     */
    @BeforeEach
    public void setUp() {
        calendarSlots = new CalendarSlots();
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("1351351"));
        letsMeetEvent.setName("AP Project");
        letsMeetEvent.setDate(LocalDate.parse("2021-05-16"));
        String timePattern = "HH:mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("07:00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("12:00", timeFormatter));

        letsMeetResponse = new LetsMeetResponse();
        letsMeetResponse.setId(Long.parseLong("1351352"));
        letsMeetResponse.setEvent(letsMeetEvent);
        letsMeetResponse.setUsername("James");
        List<String> selectedTime = Arrays.asList("10:00", "10:30");
        letsMeetResponse.setSelectedTime(selectedTime);
    }

    @Test
    public void testGetMapResponse() {
        calendarResponses = new CalendarResponses();
        int expected = calendarSlots.getTimeSlots(letsMeetEvent).size();
        assertEquals(expected, calendarResponses.getMapResponse(letsMeetEvent).size());
        assertEquals("07:00", calendarResponses.getStartTime().toString());
        assertEquals("12:00", calendarResponses.getEndTime().toString());
    }

    @Test
    public void testGetResponses() {
        calendarResponses = new CalendarResponses();
        List<LetsMeetResponse> listResponse
                = new LinkedList<>();
        listResponse.add(letsMeetResponse);
        Map<String, List<String>> map = calendarResponses.getMapResponse(letsMeetEvent);
        Map<String, List<String>> mapResult =
                calendarResponses.getResponses(listResponse, letsMeetEvent);
        assertEquals(map.size(), mapResult.size());
        assertNotEquals(map, mapResult);
    }
}
