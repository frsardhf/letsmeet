package com.application.letsmeet.controller;

import com.application.letsmeet.model.LetsMeetEvent;
import com.application.letsmeet.model.LetsMeetResponse;
import com.application.letsmeet.repository.MeetEventRepository;
import com.application.letsmeet.repository.MeetResponseRepository;
import com.application.letsmeet.service.MeetEventServiceImpl;
import com.application.letsmeet.service.MeetResponseServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LetsMeetController.class)
public class LetsMeetControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private MeetResponseServiceImpl meetResponseService;

    @MockBean
    private MeetResponseRepository meetResponseRepository;

    @MockBean
    private MeetEventServiceImpl meetEventService;

    @MockBean
    private MeetEventRepository meetEventRepository;

    private LetsMeetEvent letsMeetEvent;

    private LetsMeetResponse letsMeetResponse;

    /**
     * Set up test.
     */
    @BeforeEach
    public void setUp() {
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("135163"));
        letsMeetEvent.setName("Group Project TeemeeT");
        letsMeetEvent.setDate(LocalDate.parse("2021-06-06"));
        String timePattern = "HH:mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("13:00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("16:00", timeFormatter));

        letsMeetResponse = new LetsMeetResponse();
        letsMeetResponse.setId(Long.parseLong("1"));
        letsMeetResponse.setEvent(letsMeetEvent);
        letsMeetResponse.setUsername("Ardha");
        List<String> selectedTime = Arrays.asList("13:30", "14:00", "14:30");
        letsMeetResponse.setSelectedTime(selectedTime);
    }

    @Test
    public void getLetsMeetForm() throws Exception {
        mvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("letsMeetForm"))
                .andExpect(view().name("letsmeet/letsMeet"));
    }

    @Test
    public void postLetsMeetForm() throws Exception {
        mvc.perform(post("/")
                .param("id", "135163")
                .param("name", "Group Project TeemeeT")
                .param("date", "2021-06-06")
                .param("startTime", "13:00")
                .param("endTime", "16:00"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/details/" + letsMeetEvent.getId()));
    }

    @Test
    public void getMeetEventDetails() throws Exception {
        when(meetEventService.getMeetEventById(letsMeetEvent.getId()))
                .thenReturn(letsMeetEvent);
        mvc.perform(get("/details/135163")
                .flashAttr("event", letsMeetEvent))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getMeetEvent"))
                .andExpect(view().name("letsmeet/details"));
    }

    @Test
    public void updateMeetEventDetails() throws Exception {
        LetsMeetEvent updatedEvent = letsMeetEvent;
        String timePattern = "HH:mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        String newStartTime = "14:00";
        String newEndTime = "16:30";
        updatedEvent.setStartTime(LocalTime.parse(newStartTime, timeFormatter));
        updatedEvent.setEndTime(LocalTime.parse(newEndTime, timeFormatter));
        meetEventService.createMeetEvent(letsMeetEvent);
        when(meetEventService.getMeetEventById(letsMeetEvent.getId()))
                .thenReturn(letsMeetEvent);
        when(meetEventService.updateEvent(newStartTime, newEndTime, letsMeetEvent))
                .thenReturn(updatedEvent);
        mvc.perform(post("/update/135163")
                .param("newStartTime", "14:00")
                .param("newEndTime", "16:30"))
                .andExpect(handler().methodName("updateMeetEvent"))
                .andExpect(redirectedUrl("/details/" + letsMeetEvent.getId()));
    }

    @Test
    public void getMeetEventResponse() throws Exception {
        when(meetEventService.getMeetEventById(letsMeetEvent.getId()))
                .thenReturn(letsMeetEvent);
        mvc.perform(get("/responses/135163"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getMeetEventResponse"))
                .andExpect(view().name("letsmeet/responses"));
    }

    @Test
    public void postMeetEventResponse() throws Exception {
        Iterable<LetsMeetResponse> listResponse = Arrays.asList(letsMeetResponse);
        List<String> selectedTime = Arrays.asList("13:30", "14:00", "14:30");
        when(meetEventService.getMeetEventById(letsMeetEvent.getId()))
                .thenReturn(letsMeetEvent);
        when(meetResponseService.getListResponseByEvent(letsMeetEvent.getId()))
                .thenReturn(listResponse);
        doNothing().when(meetResponseService)
                .deleteAllResponseByUsername(listResponse, letsMeetResponse.getUsername());
        when(meetResponseService.createMeetResponse(letsMeetEvent.getId(),
                letsMeetResponse.getUsername(), selectedTime, letsMeetResponse))
                .thenReturn(letsMeetResponse);
        mvc.perform(post("/details/135163")
                .param("timeChecked","13:30")
                .param("timeChecked","14:00")
                .param("timeChecked","14:30")
                .param("name", "Ardha"))
                .andExpect(handler().methodName("letsMeetResponseSubmit"))
                .andExpect(redirectedUrl("/responses/" + letsMeetEvent.getId()));
    }
}


