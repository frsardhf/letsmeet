# Lets Meet<br>
master<br>
[![pipeline status](https://gitlab.com/frsardhf/letsmeet/badges/master/pipeline.svg)](https://gitlab.com/frsardhf/letsmeet/-/commits)
[![coverage report](https://gitlab.com/frsardhf/letsmeet/badges/master/coverage.svg)](https://gitlab.com/frsardhf/letsmeet/-/commits)

## Application Link
[letsmeetapps.herokuapp.com](https://letsmeetapps.herokuapp.com)
